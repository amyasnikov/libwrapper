
#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <dlfcn.h>

static int (*get1_int_origin)() = NULL;

int get1_int( ) {
  printf("%s\n", __FUNCTION__);
  if (get1_int_origin == NULL)
    get1_int_origin = dlsym(RTLD_NEXT, __FUNCTION__);
  return -1 * get1_int_origin();
}

void* __real_dlopen(const char *filename, int flags);

static void* (*dlopen_origin)(const char *filename, int flags) = NULL;

void* __wrap_dlopen(const char *filename, int flags) {
  printf("%s   %s\n", __FUNCTION__, filename);
  if (dlopen_origin == NULL)
    dlopen_origin = dlsym(NULL, "dlopen");
  return dlopen_origin(filename, flags); // TODO
}
