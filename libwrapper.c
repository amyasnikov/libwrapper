
#include <dlfcn.h>
#include <stddef.h>
#include <stdio.h>

#include "libwrapper.h"

#define LIBRARY "libget1_bak.so"

void* get_function(const char* lib_name, const char* func_name) {
  void *hdl = dlopen(lib_name, RTLD_LAZY);
  if (NULL == hdl) return NULL;
  return dlsym(hdl, func_name);
}

int get1_int( ) {
  void* func = get_function(LIBRARY, __FUNCTION__);
  if (NULL != func)
    return ((int(*)()) func)() * 100; // wrapper_test
  return -1;
}

double get1_double( ) {
  void* func = get_function(LIBRARY, __FUNCTION__);
  if (NULL != func)
    return ((double(*)()) func)() * 100; // wrapper_test
  return -1;
}
