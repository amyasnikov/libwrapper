
#include <stdio.h>
#include "libget1.h"

int get1_int( ) {
    printf("%s\n", __FUNCTION__);
    return 1;
}

double get1_double( ) {
    printf("%s\n", __FUNCTION__);
    return 1.5;
}
