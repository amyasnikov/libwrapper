
#include "libget1.h"
#include <stdio.h>
#include <dlfcn.h>

#define LIBGET1 "libget1.so"

int main( ) {

    int (*func)();
    void *hdl = dlopen(LIBGET1, RTLD_LAZY);
    if (NULL != hdl)
        func = dlsym(hdl, "get1_int"); // NULL <=> hdl

    printf("XXX 1 \n");
    int    i = (NULL != func) ? func() : 0;
    printf("XXX 2 \n");
    int    j = get1_int();
    double d = get1_double();
    printf("int i = [%d]\n", i);
    printf("int j = [%d]\n", j);
    printf("int d = [%f]\n", d);

    return 0;
}
