
all: libget1 libget2 main

libget1:
	gcc -c -fpic -shared libget1.c -o libget1.o
	gcc -shared -o libget1.so libget1.o
	rm -f libget1.o

libget2:
	gcc -Wl,-wrap,dlopen -shared -fpic -ldl -O0 -o libget2.so libget2.c

main: libget1
	gcc -Wl,-wrap,dlopen -Wl,-rpath=. main.c -Bdynamic -L. -lget1 -lget2 -ldl -o main



wrapper: libget1
	gcc -c -fpic -shared libwrapper.c -o libwrapper.o
	gcc -shared -o libwrapper.so libwrapper.o -ldl -L. -lget1
	rm -f libwrapper.o
	mv libget1.so libget1_bak.so
	cp libwrapper.so libget1.so



run:
	./main

clean:
	rm -rf libget1.so
	rm -rf libget2.so
	rm -rf libget1_bak.so
	rm -rf libwrapper.so
	rm -rf main
